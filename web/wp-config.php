<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(&ls]p6c$7DS)+AW3lv$Sq^)$<)R/c2co04;`&R(q=,0(/kP|tN|N!g_X?KQnHM[');
define('SECURE_AUTH_KEY',  '1=`,C n{D,OAM~gQ<HI|{Wx*mR9-^V=<Fw{,Qjr0KiBH<~t! A=oI$]VX_rK<opT');
define('LOGGED_IN_KEY',    'Yh:I-(_CU67vlOiV%.&M/::[0HD {T4{>s7D|e*e 9Wu`+ZvjhT[8C%Ms|Qimb.X');
define('NONCE_KEY',        'jE}]NrLbv-6DiVwB$89~l_^)H/8+`/Mx)rBgCP8Q/e7#?.K$Je|>ghE*El@|:P(*');
define('AUTH_SALT',        'M)R*5r)Gee&WHxW.5m]*:6J#^`5j->aas^]Zh3GX45-@UW+#Bv0)A90u%&i?T6r}');
define('SECURE_AUTH_SALT', 'Ggv`]!K4(Cq>5s@M_QF:Si<gi~cX?l=Vb_) bGCQ3<>D;%/~6oc<l$f;~,8pMjkq');
define('LOGGED_IN_SALT',   'uSfMNJX2l.SC$k_g~$F4*=wFL<ADKaY16FZDL[Ra(QwF:cSO3HkW.?DtX_@AExGN');
define('NONCE_SALT',       'aYsu~i@rZ#*~xcfQ,{Ga-%+bnZ|wN$](Vw0Fjrt7cD!pdl0}T.GGjgxAQ7h^X]V%');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
