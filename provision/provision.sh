#!/usr/bin/env bash


echo "Update packages"
cp -r /vagrant/provision/linuxConfiguration/etc/apt/* /etc/apt
apt-get update -q

echo "Installing packages"
apt-get install sudo wget curl git-core vim elinks apache2  php-common \
        libapache2-mod-php php-dev php-cli php-curl \
        php-gd php-mcrypt php-mysql php-xml php-json -y
echo "Copy apache2 configuration"
cp -r /vagrant/provision/linuxConfiguration/etc/apache2/* /etc/apache2
a2enmod rewrite
service apache2 restart
if [ ! -f /usr/sbin/mysqld ]; then
    echo "Installing packages - mysql"
    DEBIAN_FRONTEND=noninteractive apt-get  -y install mysql-server
    mysqladmin -u root password root
fi
